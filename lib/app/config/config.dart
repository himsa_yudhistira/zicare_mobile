class Config {
  static const String API_URL = 'https://zpi-dev.zicare.id';
  static const String LOGIN = '/api/v1/auth/token';
  static const String REGISTER = '/api/v1/auth/register';
  static const String HOME = '/api/v1/users/me/';
}
