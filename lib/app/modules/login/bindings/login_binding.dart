import 'package:get/get.dart';
import 'package:zicare/app/data/service/login_service.dart';

import '../controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginService>(() => LoginService());
    Get.lazyPut(() => LoginController(loginService: Get.find()));
  }
}
