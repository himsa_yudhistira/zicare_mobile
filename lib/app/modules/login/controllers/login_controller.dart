import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zicare/app/data/model/login_model.dart';
import 'package:zicare/app/data/service/login_service.dart';
import 'package:zicare/app/routes/app_pages.dart';
import 'package:zicare/app/utils/widgets.dart';

class LoginController extends GetxController with SingleGetTickerProviderMixin {
  LoginController({required this.loginService});

  final LoginService loginService;

  late AnimationController animationController;
  late AnimationController animationController2;

  final emailValue = ''.obs;
  final passwordValue = ''.obs;
  final token = '';

  @override
  void onInit() {
    super.onInit();
    animationController = AnimationController(
      duration: Duration(seconds: 4),
      vsync: this,
    )
      ..forward()
      ..repeat(reverse: true);
    animationController2 = AnimationController(
      duration: Duration(seconds: 3),
      vsync: this,
    )
      ..forward()
      ..repeat(reverse: true);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    animationController.dispose();
    animationController2.dispose();
  }

  saveValueEmail(String value) {
    emailValue.value = value;
  }

  saveValuePassword(String value) {
    passwordValue.value = value;
  }

  login() {
    if (emailValue.value != '' && passwordValue.value != '') {
      final box = GetStorage();
      box.write('isLogin', false);
      showLoadingDialog();
      loginService.postAuth(emailValue.value, passwordValue.value).then((_res) {
        if (Get.isDialogOpen!) Get.back();
        if (_res.statusCode > 200) {
          showErrorSnackBar(_res.body['message']);
        } else {
          var response = Login.fromJson(_res.body);
          var data = response.accessToken;
          if (data != null) {
            box.write('token', data);
            box.write('isLogin', true);
            Get.offNamed(Routes.HOME);
          }
        }
      });
    } else {
      showWarningSnackBar('Please fill all field');
    }
  }

  notImplemented() {
    showWarningSnackBar('Not Implemented yet !');
  }

  toRegister() {
    Get.offNamed(Routes.REGISTER);
  }
}
