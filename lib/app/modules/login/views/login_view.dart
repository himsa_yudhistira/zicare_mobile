import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zicare/app/utils/widgets.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: Get.height,
        child: Stack(
          children: [
            Positioned(
              top: -Get.width * .2,
              left: -Get.width * .2,
              child: Container(
                height: Get.width * .7,
                width: Get.width * .7,
                alignment: Alignment.center,
                child: circle(controller.animationController),
              ),
            ),
            Positioned(
              top: Get.width * .2,
              right: -30,
              child: Container(
                height: Get.width * .3,
                width: Get.width * .3,
                alignment: Alignment.center,
                child: circle(controller.animationController2),
              ),
            ),
            Positioned.fill(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Center(
                      child: Text(
                        'Welcome Back',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TextFormField(
                              decoration:
                                  textFieldDecoration('username / email'),
                              onChanged: (value) =>
                                  controller.saveValueEmail(value),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              obscureText: true,
                              decoration: textFieldDecoration('password'),
                              onChanged: (value) =>
                                  controller.saveValuePassword(value),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                    onPressed: () => controller.login(),
                                    child: Container(
                                      height: 50.0,
                                      child: Center(child: Text('SIGN IN')),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 40,
                                ),
                                TextButton(
                                  onPressed: () => controller.notImplemented(),
                                  child: Container(
                                    height: 50.0,
                                    child: Center(
                                        child: Text('Forgot Password ?')),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextButton(
                              onPressed: () => controller.toRegister(),
                              child: Container(
                                height: 50.0,
                                child: Center(
                                    child: Text(
                                        "Don't have an account ? Sign Up")),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
