import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Center(
            child: controller.logo(),
          ),
          Obx(
            () => Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text('version ${controller.version.value}'),
            ),
          ),
        ],
      ),
    );
  }
}
