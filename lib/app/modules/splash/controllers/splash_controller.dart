import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info/package_info.dart';
import 'package:zicare/app/routes/app_pages.dart';

class SplashController extends GetxController {
  final logo = FlutterLogo(
    size: 100.0,
  ).obs;

  final version = ''.obs;
  @override
  void onInit() {
    super.onInit();
    init();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  init() async {
    var packageInfo = await PackageInfo.fromPlatform();
    version.value = '${packageInfo.version}+${packageInfo.buildNumber}';
    final box = GetStorage();
    var isLogin = box.read('isLogin') ?? false;
    var token = box.read('token') ?? '';
    print('$isLogin   $token');
    if (isLogin) {
      if (token != '') {
        Get.offNamed(Routes.HOME);
      } else {
        Get.offNamed(Routes.LOGIN);
      }
    } else {
      Get.offNamed(Routes.LOGIN);
    }
  }
}
