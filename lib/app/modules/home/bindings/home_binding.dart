import 'package:get/get.dart';
import 'package:zicare/app/data/service/home_service.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeService>(() => HomeService());
    Get.lazyPut(() => HomeController(homeService: Get.find()));
  }
}
