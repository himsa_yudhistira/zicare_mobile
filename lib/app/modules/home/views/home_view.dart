import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('HomeView'),
      //   centerTitle: true,
      // ),
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                flex: 5,
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.deepPurple[800]!,
                        Colors.deepPurpleAccent
                      ],
                    ),
                  ),
                  child: Column(children: [
                    SizedBox(
                      height: 110.0,
                    ),
                    CircleAvatar(
                      radius: 65.0,
                      // backgroundImage: AssetImage('assets/erza.jpg'),
                      backgroundColor: Colors.white,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Obx(() => Text(controller.fullName.value,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ))),
                    SizedBox(
                      height: 10.0,
                    ),
                    Obx(() => Text(
                          controller.email.value,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          ),
                        ))
                  ]),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  color: Colors.grey[200],
                  child: Center(
                    child: TextButton(
                      onPressed: () => controller.logOut(),
                      child: Container(
                        height: 50.0,
                        child: Center(child: Text("Sign Out")),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.45,
              left: 20.0,
              right: 20.0,
              child: Card(
                  child: Padding(
                padding: EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        child: Column(
                      children: [
                        Text(
                          'self isolation',
                          style: TextStyle(
                              color: Colors.grey[400], fontSize: 14.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Obx(() => Text(
                              controller.self.value,
                              style: TextStyle(
                                fontSize: 15.0,
                              ),
                            ))
                      ],
                    )),
                    Container(
                      child: Column(children: [
                        Text(
                          'rdt',
                          style: TextStyle(
                              color: Colors.grey[400], fontSize: 14.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Obx(() => Text(
                              controller.rdt.value,
                              style: TextStyle(
                                fontSize: 15.0,
                              ),
                            ))
                      ]),
                    ),
                    Container(
                        child: Column(
                      children: [
                        Text(
                          'hospital id',
                          style: TextStyle(
                              color: Colors.grey[400], fontSize: 14.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Obx(() => Text(
                              controller.hospital.value,
                              style: TextStyle(
                                fontSize: 15.0,
                              ),
                            ))
                      ],
                    )),
                  ],
                ),
              )))
        ],
      ),
    );
  }
}
