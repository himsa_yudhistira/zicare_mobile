import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zicare/app/data/model/register_model.dart';
import 'package:zicare/app/data/service/home_service.dart';
import 'package:zicare/app/routes/app_pages.dart';
import 'package:zicare/app/utils/widgets.dart';

class HomeController extends GetxController {
  HomeController({required this.homeService});

  final HomeService homeService;
  final box = GetStorage();
  final user = User().obs;
  final fullName = 'User'.obs;
  final email = 'user@email.com'.obs;
  final self = ''.obs;
  final rdt = ''.obs;
  final hospital = ''.obs;
  @override
  void onInit() {
    super.onInit();
    getData();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getData() {
    homeService.getHome().then((_res) {
      if (_res.statusCode > 200) {
        showErrorSnackBar(_res.body['message']);
      } else {
        var response = Register.fromJson(_res.body);
        user.value = response.data!.user!;
        fullName.value = user.value.fullName!;
        email.value = user.value.email!;
        self.value = user.value.covid19SelfIsolation.toString();
        rdt.value = user.value.covid19Rdt.toString();
        hospital.value = user.value.defaultHospitalId.toString();
      }
    });
  }

  void logOut() {
    box.write('isLogin', false);
    box.write('token', '');
    Get.offNamed(Routes.LOGIN);
  }
}
