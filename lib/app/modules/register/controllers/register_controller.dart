import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zicare/app/data/service/register_service.dart';
import 'package:zicare/app/routes/app_pages.dart';
import 'package:zicare/app/utils/widgets.dart';

class RegisterController extends GetxController
    with SingleGetTickerProviderMixin {
  RegisterController({required this.registerService});

  final RegisterService registerService;
  late AnimationController animationController;
  late AnimationController animationController2;

  final emailValue = ''.obs;
  final userNameValue = ''.obs;
  final fullNameValue = ''.obs;
  final passwordValue = ''.obs;

  @override
  void onInit() {
    super.onInit();
    animationController = AnimationController(
      duration: Duration(seconds: 4),
      vsync: this,
    )
      ..forward()
      ..repeat(reverse: true);
    animationController2 = AnimationController(
      duration: Duration(seconds: 3),
      vsync: this,
    )
      ..forward()
      ..repeat(reverse: true);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    animationController.dispose();
    animationController2.dispose();
  }

  saveValueEmail(String value) {
    emailValue.value = value;
  }

  saveValueUsername(String value) {
    userNameValue.value = value;
  }

  saveValueFullName(String value) {
    fullNameValue.value = value;
  }

  saveValuePassword(String value) {
    passwordValue.value = value;
  }

  register() {
    if (fullNameValue.value != '' &&
        fullNameValue.value != '' &&
        emailValue.value != '' &&
        passwordValue.value != '') {
      final box = GetStorage();
      box.write('isLogin', false);
      showLoadingDialog();
      registerService
          .postRegister(fullNameValue.value, userNameValue.value,
              emailValue.value, passwordValue.value)
          .then((_res) {
        if (Get.isDialogOpen!) Get.back();
        if (_res.statusCode > 200) {
          showErrorSnackBar(
              '${_res.body['message']} ${_res.body['field_errors']}');
        } else {
          Get.offNamed(Routes.LOGIN);
        }
      });
    } else {
      showWarningSnackBar('Please fill all field');
    }
  }

  notImplemented() {
    showWarningSnackBar('Not Implemented yet !');
  }

  toLogin() {
    Get.offNamed(Routes.LOGIN);
  }
}
