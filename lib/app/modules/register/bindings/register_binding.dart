import 'package:get/get.dart';
import 'package:zicare/app/data/service/register_service.dart';

import '../controllers/register_controller.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegisterService>(() => RegisterService());
    Get.lazyPut(() => RegisterController(registerService: Get.find()));
  }
}
