import 'package:zicare/app/config/config.dart';

import 'base_service.dart';

class RegisterService extends BaseService {
  Future<dynamic> postRegister(
      String fullName, String username, String email, String password) async {
    final Map<String, dynamic> body = {
      "full_name": fullName,
      "username": username,
      "email": email,
      "password": password
    };
    var res;
    try {
      res = await post(Config.REGISTER, body);
      return res;
    } catch (e) {
      print(e);
      return e;
    }
  }
}
