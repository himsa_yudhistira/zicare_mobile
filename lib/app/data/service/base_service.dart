import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get_storage/get_storage.dart';
import 'package:zicare/app/config/config.dart';

class BaseService extends GetConnect {
  @override
  void onInit() {
    // add your local storage here to load for every request
    final box = GetStorage();
    var isLogin = box.read('isLogin') ?? false;
    var token = box.read('token') ?? '';
    //base_url
    httpClient.baseUrl = Config.API_URL;
    //additional
    httpClient.defaultContentType = "application/json";
    httpClient.timeout = Duration(seconds: 8);
    httpClient.addResponseModifier((request, response) async {
      print('BASE RESPONSE');
      print(response.body);
    });
    // request modifier
    httpClient.addRequestModifier((Request request) async {
      // add request here
      return request;
    });

    // auth token header
    if (isLogin) {
      if (token != '') {
        var headers = {'Authorization': "Bearer $token"};
        httpClient.addAuthenticator((Request request) async {
          request.headers.addAll(headers);
          return request;
        });
      }
    }

    super.onInit();
  }
}
