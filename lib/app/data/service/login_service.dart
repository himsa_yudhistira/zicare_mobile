import 'package:zicare/app/config/config.dart';

import 'base_service.dart';

class LoginService extends BaseService {
  Future<dynamic> postAuth(String username, String password) async {
    final Map<String, dynamic> body = {
      "username": username,
      "password": password,
      "grant_type": "password"
    };
    var res;
    try {
      res = await post(Config.LOGIN, body);
      return res;
    } catch (e) {
      print(e);
      return e;
    }
  }
}
