import 'package:zicare/app/config/config.dart';

import 'base_service.dart';

class HomeService extends BaseService {
  Future<dynamic> getHome() async {
    var res;
    try {
      res = await get(Config.HOME);
      return res;
    } catch (e) {
      print(e);
      return e;
    }
  }
}
