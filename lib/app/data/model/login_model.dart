// To parse this JSON data, do
//
//     final login = loginFromJson(jsonString);

import 'dart:convert';

class Login {
  Login({
    this.tokenType,
    this.accessToken,
    this.refreshToken,
    this.expiresIn,
  });

  String? tokenType;
  String? accessToken;
  String? refreshToken;
  int? expiresIn;

  factory Login.fromRawJson(String str) => Login.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        tokenType: json["token_type"] == null ? null : json["token_type"],
        accessToken: json["access_token"] == null ? null : json["access_token"],
        refreshToken:
            json["refresh_token"] == null ? null : json["refresh_token"],
        expiresIn: json["expires_in"] == null ? null : json["expires_in"],
      );

  Map<String, dynamic> toJson() => {
        "token_type": tokenType == null ? null : tokenType,
        "access_token": accessToken == null ? null : accessToken,
        "refresh_token": refreshToken == null ? null : refreshToken,
        "expires_in": expiresIn == null ? null : expiresIn,
      };
}
