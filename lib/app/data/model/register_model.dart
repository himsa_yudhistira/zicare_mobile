// To parse this JSON data, do
//
//     final register = registerFromJson(jsonString);

import 'dart:convert';

class Register {
  Register({
    this.success,
    this.message,
    this.data,
  });

  bool? success;
  String? message;
  Data? data;

  factory Register.fromRawJson(String str) =>
      Register.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Register.fromJson(Map<String, dynamic> json) => Register(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data?.toJson(),
      };
}

class Data {
  Data({
    this.user,
  });

  User? user;

  factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "user": user == null ? null : user?.toJson(),
      };
}

class User {
  User({
    this.id,
    this.email,
    this.username,
    this.fullName,
    this.avatar,
    this.personId,
    this.fcmToken,
    this.covid19SelfIsolation,
    this.covid19Rdt,
    this.defaultHospitalId,
    this.lastLoginAt,
    this.emailActivatedAt,
    this.createdAt,
    this.updatedAt,
  });

  String? id;
  String? email;
  String? username;
  String? fullName;
  dynamic avatar;
  dynamic personId;
  dynamic fcmToken;
  dynamic covid19SelfIsolation;
  dynamic covid19Rdt;
  dynamic defaultHospitalId;
  dynamic lastLoginAt;
  dynamic emailActivatedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        username: json["username"] == null ? null : json["username"],
        fullName: json["full_name"] == null ? null : json["full_name"],
        avatar: json["avatar"],
        personId: json["person_id"],
        fcmToken: json["fcm_token"],
        covid19SelfIsolation: json["covid19_self_isolation"],
        covid19Rdt: json["covid19_rdt"],
        defaultHospitalId: json["default_hospital_id"],
        lastLoginAt: json["last_login_at"],
        emailActivatedAt: json["email_activated_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "username": username == null ? null : username,
        "full_name": fullName == null ? null : fullName,
        "avatar": avatar,
        "person_id": personId,
        "fcm_token": fcmToken,
        "covid19_self_isolation": covid19SelfIsolation,
        "covid19_rdt": covid19Rdt,
        "default_hospital_id": defaultHospitalId,
        "last_login_at": lastLoginAt,
        "email_activated_at": emailActivatedAt,
        "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
      };
}
