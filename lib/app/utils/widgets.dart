import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showErrorSnackBar(String e) {
  Get.rawSnackbar(
    snackStyle: SnackStyle.GROUNDED,
    backgroundColor: Colors.red,
    borderRadius: 5,
    messageText: Text(
      e,
      maxLines: 30,
      style: TextStyle(
        color: Colors.white,
        fontSize: 16,
      ),
    ),
    isDismissible: true,
    forwardAnimationCurve: Curves.easeInOut,
    barBlur: 20,
    snackPosition: SnackPosition.BOTTOM,
    margin: EdgeInsets.zero,
  );
}

void showSuccessSnackBar(String body) {
  Get.rawSnackbar(
    snackStyle: SnackStyle.GROUNDED,
    backgroundColor: Colors.green,
    snackPosition: SnackPosition.BOTTOM,
    borderRadius: 5,
    messageText: Text(
      body,
      maxLines: 30,
      style: TextStyle(
        color: Colors.white,
        fontSize: 16,
      ),
    ),
    isDismissible: true,
    forwardAnimationCurve: Curves.easeInOut,
    barBlur: 20,
    margin: EdgeInsets.zero,
  );
}

void showWarningSnackBar(String body) {
  Get.rawSnackbar(
    snackStyle: SnackStyle.GROUNDED,
    backgroundColor: Colors.amber,
    snackPosition: SnackPosition.BOTTOM,
    borderRadius: 5,
    messageText: Text(
      body,
      maxLines: 30,
      style: TextStyle(
        color: Colors.white,
        fontSize: 16,
      ),
    ),
    isDismissible: true,
    forwardAnimationCurve: Curves.easeInOut,
    barBlur: 20,
    margin: EdgeInsets.zero,
  );
}

void showLoadingDialog() {
  Get.dialog(
    AlertDialog(
      scrollable: true,
      content: Column(
        children: [
          Text(
            'Please wait',
          ),
          SizedBox(
            height: 20.0,
          ),
          CircularProgressIndicator(),
        ],
      ),
    ),
    barrierDismissible: false,
  );
}

InputDecoration textFieldDecoration(String label) {
  return InputDecoration(
    labelText: label,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(5.0)),
    ),
    filled: true,
  );
}

Widget circle(AnimationController animationController) {
  return AnimatedBuilder(
    animation: animationController,
    builder: (context, child) {
      return Container(
        decoration: ShapeDecoration(
          shape: CircleBorder(),
        ),
        child: Padding(
          padding: EdgeInsets.all(
            8.0 * animationController.value,
          ),
          child: child,
        ),
      );
    },
    child: Container(
      padding: EdgeInsets.all(8.0),
      decoration: ShapeDecoration(
        shape: CircleBorder(),
        gradient: LinearGradient(colors: [
          Color(0xffFD5E3D),
          Color(0xffC43990),
        ], begin: Alignment.topLeft, end: Alignment.bottomRight),
      ),
    ),
  );
}
